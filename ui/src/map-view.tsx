import * as React from "react"
import {ChangeEvent, useEffect, useRef, useState} from "react"
import {Feature, Map, MapBrowserEvent, Overlay, View} from "ol";
import {fromLonLat, transform} from "ol/proj";
import TileLayer from "ol/layer/Tile";
import {OSM, Vector} from "ol/source";

import "ol/ol.css";
import {FeatureLike} from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import {GeoJSON} from "ol/format";
import {Icon, Style} from "ol/style";
import {defaults as DefaultControls, MousePosition, OverviewMap, ScaleLine, ZoomSlider} from "ol/control";
import GeometryType from "ol/geom/GeometryType";
import {Coordinate} from "ol/coordinate";
import {Pixel} from "ol/pixel";

import '../styles.css'

interface IGeometry {
    coordinates: Coordinate,
    type: GeometryType
}

interface IFeature {
    type: string
    geometry: IGeometry,
    properties: {}
}

const geoJson = new GeoJSON();

const mapPinStyle = new Style({
    image: new Icon({
        src: "/img/map-pin-blue.png",
        scale: 25 / 50,
        anchor: [0.5, 1.0]
    })
});

export const MapView: React.FC = () => {
    const [map, setMap] = useState<Map | undefined>(undefined)
    const [featureLayer, setFeatureLayer] = useState<VectorLayer | undefined>()
    const [features, setFeatures] = useState<FeatureLike[]>([])
    const [name, setName] = useState<string | null>("marker")
    const mapRef = useRef<Map | undefined>(undefined)
    const nameRef = useRef<string | undefined>(undefined)
    const popupDivRef = useRef<HTMLDivElement | undefined>(undefined)
    const popupOverlayRef = useRef<Overlay | undefined>(undefined)

    useEffect(() => {
        // get updated value of name everytime it changes
        nameRef.current = name;
    }, [name])

    useEffect(() => {
        const map = new Map({
            target: "map",
            layers: [
                new TileLayer({
                    source: new OSM()
                })
            ],
            // Added map controls
            controls: DefaultControls().extend([
                new ZoomSlider(),
                new MousePosition(),
                new ScaleLine(),
                new OverviewMap()
            ]),
            view: new View({
                center: fromLonLat([-0.023758, 51.547504]),
                projection: 'EPSG:3857',
                zoom: 13,
            })
        })

        const popup = new Overlay({
            element: popupDivRef.current
        })

        map.on("click", onMapClick);

        // refs so it can be accessed in onMapClick callback function
        mapRef.current = map;
        popupOverlayRef.current = popup;

        setMap(map)
        loadFeatureData()
    }, [])

    useEffect(() => {
        if (map) {
            setFeatureLayer(addFeatureLayer(featureLayer, features))
        }
    }, [map, features])

    const loadFeatureData = () => {
        fetch("/api/geo-json")
            .then(response => response.json())
            .then(json => {
                setFeatures(geoJson.readFeatures(json, {
                        dataProjection: 'EPSG:4326',
                        featureProjection: 'EPSG:3857'
                    })
                )
            })
    }

    const addFeatureLayer = (previousLayer: VectorLayer, features: FeatureLike[]): VectorLayer => {
        const newLayer = previousLayer ? previousLayer : new VectorLayer({
            style: mapPinStyle,
            visible: true
        });

        if (previousLayer != undefined) {
            previousLayer.getSource().clear();
        } else {
            map.addLayer(newLayer);
        }

        (newLayer as any).tag = "features";

        const source = new Vector({
            format: geoJson,
            features: features as Feature<any>[],
        });

        newLayer.setSource(source);

        return newLayer
    }

    const getNameFromFeature = (pixel: Pixel) => {
        let name: string = "";
        // if pixel has feature, get the name
        mapRef.current.forEachFeatureAtPixel(pixel, (feature) => {
            if (feature) name = feature.get("name");
        })
        return name;
    }

    const onMapClick = (e: MapBrowserEvent) => {
        const name = getNameFromFeature(e.pixel);

        // if name exists, display popup with the name for that coordinate or else add new coordinate
        if (name.length > 0) {
            // display feature info
            popupDivRef.current.innerHTML = name || "Point Name: N/A";

            // display popup on position of user click
            popupOverlayRef.current.setPosition(e.coordinate);

            // add overlay to map
            mapRef.current.addOverlay(popupOverlayRef.current);

        } else {

            // transform coordinate to map projection
            let latLong = transform(e.coordinate, 'EPSG:3857', 'EPSG:4326');

            // create feature with coordinates and name
            let feature: IFeature = {
                type: "Feature",
                geometry: {
                    coordinates: latLong,
                    type: GeometryType.POINT
                },
                properties: {
                    name: nameRef.current
                }
            }

            // headers for POST request
            const requestOptions: RequestInit = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(feature)
            }

            // POST request to store the new coordinate
            fetch("/api/geo-json/add", requestOptions)
                .then(() => {
                    loadFeatureData()
                })
        }
    }

    const handleNameChange = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value)
    }

    return <div>
        <label>
            Name of Point:
            <input type="text" name="name" value={name} onChange={handleNameChange}/>
        </label>
        <div id="map"/>
        <div ref={popupDivRef} className="feature-information-popup"/>
    </div>
}