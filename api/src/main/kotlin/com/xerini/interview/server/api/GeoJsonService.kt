@file:Suppress("unused")

package com.xerini.interview.server.api

import com.xerini.interview.util.GsonUtil.gson
import java.awt.geom.GeneralPath
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.ArrayList
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.Executors
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response


@Path("/geo-json")
class GeoJsonService {
    private val filePath = "api/src/main/kotlin/com/xerini/interview/data/map.geojson"
//    private val allCoordinates = CopyOnWriteArrayList<List<Double>>()
    private var geoJsonFeatureList: MutableList<GeoJsonFeature> = mutableListOf()

    @GET
    @Produces(APPLICATION_JSON)
    fun getGeoJson(): GeoJsonObject {

        // get geo json data from file
        val geoJsonFromFile = gson.fromJson(
            FileReader(filePath),
            GeoJsonObject::class.java
        )

        // check if file has any data
        if (geoJsonFromFile !== null)
        // assign geojson data to features list
            geoJsonFeatureList = geoJsonFromFile.features as MutableList<GeoJsonFeature>

        return GeoJsonObject(geoJsonFeatureList)
    }

    @Path("/add")
    @POST
    @Consumes(APPLICATION_JSON)
    fun addPoint(feature: GeoJsonFeature): Response {

        // add feature to feature list
        geoJsonFeatureList.add(feature)
        val geoJsonObject = GeoJsonObject(geoJsonFeatureList)

        // write geoJson to file
        val toJson = gson.toJson(geoJsonObject)
        File(filePath).writeText(toJson)

        return Response.ok().build()
    }
}

data class GeoJsonObject(val features: List<GeoJsonFeature>) {
    val type: String = "FeatureCollection"
}

data class GeoJsonFeature(val geometry: GeometryData?, val properties: Map<String, Any?> = emptyMap()) {
    val type: String = "Feature"
}

data class GeometryData(val coordinates: List<Double>) {
    val type: String = "Point"
}